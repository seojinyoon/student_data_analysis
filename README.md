# Student_data_analysis

해야할것  

1. home directory 경로를 적절하게 수정해야 합니다.
2. Model 저장소 경로를 적절하게 수정해야 합니다.

**Analysis.ipynb** 
- 해당 파일의 os.chdir 코드를 자신의 컴퓨터의 적절한 경로로 설정해주세요.(프로젝트의 경로)
- 해당 파일의 model_dir_path 코드를 적절한 경로로 설정해주세요.(어디에 모델을 저장할지)

**prj_info.py**
- prj_home_path를 적절한 경로로 설정해주세요.(프로젝트의 경로)
	
